#include "phonebook_global.h"

#include "mainwindow.h"

#include <QApplication>
#include <QSqlQuery>
#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QSqlError>
#include <QDebug>


#define DB_TYPE "QSQLITE"
#define DB_NAME "phonebook.sqlite"

#define DB_TABLE_CONTACTS "contacts"



using GenericPairingPhonebook::MainWindow;



void initializeDB(){
    QSqlQuery query;
    query.exec("create table " DB_TABLE_CONTACTS " ('Contact name' text primary key, "
               "integer hasPairing, 'Full name' text, 'Phone number' text, Email text)");
}




int main(int argc, char *argv[])
{
    QApplication a(argc, argv);


    QSqlDatabase db = QSqlDatabase::addDatabase(DB_TYPE);
    db.setDatabaseName(DB_NAME);
    if (!db.open()){
        qDebug() << a.tr("Could not create or open database: ") << db.lastError();
        exit(1);
    }
    if (!db.tables().contains(DB_TABLE_CONTACTS)){ // the database was newly created (or is corrupt)
        initializeDB();
    }
    QSqlTableModel dbModel(0,db);
    dbModel.setTable(DB_TABLE_CONTACTS);
    dbModel.select();

    MainWindow w(&dbModel);
    w.show();

    return a.exec();
}
