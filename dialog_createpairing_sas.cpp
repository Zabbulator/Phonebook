#include "dialog_createpairing_sas.h"
#include "ui_dialog_createpairing_sas.h"

#include "../GenericPairingLibrary/genericpairing.h"

#include <QDataStream>
#include <QByteArray>
#include <QDebug>

using GenericPairingPhonebook::Dialog_CreatePairing_Sas;

Dialog_CreatePairing_Sas::Dialog_CreatePairing_Sas(QZeroConf* zeroConf,
                                                   bool isClient,
                                                   bool mustPublishMdnssdService,
                                                   const QByteArray& pairingPartnerName,
                                                   const QByteArray& pairingPartnerIP,
                                                   uint16_t pairingPartnerPort,
                                                   QWidget *parent) :
    zeroConf(zeroConf),
    isClient(isClient),
    mustPublishMdnssdService(mustPublishMdnssdService),
    pairingPartnerName(pairingPartnerName),
    pairingPartnerIP(pairingPartnerIP),
    pairingPartnerPort(pairingPartnerPort),
    QDialog(parent),
    ui(new Ui::Dialog_CreatePairing_Sas)
{
    ui->setupUi(this);

    if (mustPublishMdnssdService){
        qsrand(time(nullptr));
        int randomInt = qrand();
        QByteArray byteArray = QByteArray::fromRawData((const char*) &randomInt, sizeof(randomInt));
        QString mdnssdServiceHostname = QString(byteArray.toHex()); // Random sizeof(int) sized host name as hex
        zeroConf->startServicePublish(mdnssdServiceHostname.toStdString().c_str(),
                                      GenericPairing::MdnssdServiceType.c_str(),
                                      "local",
                                      GenericPairing::DefaultPort);
        connect(this, &Dialog_CreatePairing_Sas::finished, zeroConf, &QZeroConf::stopServicePublish);
        ui->label_mdnssdServiceName->setText("Your mDNS-SD service name: " + mdnssdServiceHostname);
    }
    else
        ui->label_mdnssdServiceName->hide();

    ui->label_description->setText("Connecting to pairing daemon...");
    remainingTime = GenericPairing::DefaultWaitTime/1000; // s instead of msec
    ui->label_remainingTime->setText(QString::number(remainingTime) + "s");

    timer.setInterval(1000);
    connect(&timer, &QTimer::timeout, [this]{
        if (--remainingTime == 0)
            done(QDialog::Rejected);
        else
            ui->label_remainingTime->setText(QString::number(remainingTime) + "s");
    });
    timer.start();

#ifdef Q_OS_ANDROID
    ipcSocket = new QTcpSocket;
    connect(ipcSocket, &QTcpSocket::connected, this, &Dialog_CreatePairing_Sas::sendRequest);
    ipcSocket->connectToHost("localhost",GenericPairing::DefaultLoopbackIpcPort);
#else
    ipcSocket = new QLocalSocket;
    connect(ipcSocket, &QLocalSocket::connected, this, &Dialog_CreatePairing_Sas::sendRequest);
    ipcSocket->connectToServer(QString::fromStdString(GenericPairing::IpcServerName));
#endif // Q_OS_ANDROID
}

Dialog_CreatePairing_Sas::~Dialog_CreatePairing_Sas()
{
    delete ui;
    if (this->result() == QDialog::Accepted){
        QDataStream localStream(ipcSocket);
        localStream << (GenericPairing::message_t) GenericPairing::IpcMessage::Success
                    << "\n";
    } else if (this->result() == QDialog::Rejected){
        QDataStream localStream(ipcSocket);
        localStream << (GenericPairing::message_t) GenericPairing::IpcMessage::Failure
                    << "\n";
    }

    ipcSocket->flush();
    delete ipcSocket;
}




void Dialog_CreatePairing_Sas::sendRequest(){
    GenericPairing::IpcMessage message = isClient ? GenericPairing::IpcMessage::Request_SendPairingRequest
                                                  : GenericPairing::IpcMessage::Request_WaitForPairingRequest;

    connect(ipcSocket, &QAbstractSocket::readyRead, this, &Dialog_CreatePairing_Sas::readSasMessage);

    QDataStream localStream(ipcSocket);
    localStream << (GenericPairing::message_t) message
                << pairingPartnerName;
    if (isClient)
        localStream << pairingPartnerIP << pairingPartnerPort;
    localStream << "\n";

    ui->label_description->setText("Waiting for SAS from pairing daemon...");
    remainingTime = isClient ? GenericPairing::DefaultWaitTime/1000
                             : GenericPairing::DefaultListenTime/1000;
    ui->label_remainingTime->setText(QString::number(remainingTime) + "s");
}

void Dialog_CreatePairing_Sas::readSasMessage(){
    if (!ipcSocket->canReadLine())
        return;

    GenericPairing::message_t message_raw;
    QByteArray sas_raw;
    QDataStream localStream(ipcSocket);
    localStream >> message_raw >> sas_raw;
    ipcSocket->readAll(); // Get rid of trailing \n
    GenericPairing::IpcMessage message = (GenericPairing::IpcMessage) message_raw;
    QString sas = QString::fromStdString(sas_raw.toStdString());

    if (message != GenericPairing::IpcMessage::Success)
        done(QDialog::Rejected);

    // If we get here, we got an SAS, show it and wait for the user to confirm
    ui->label_description->setText("Is the SAS shown below the same as your friend's SAS?");
    remainingTime = GenericPairing::DefaultListenTime/1000;
    ui->label_remainingTime->setText(QString::number(remainingTime) + "s");
    ui->label_sas->setText("SAS: " + sas);

    ui->buttonBox->addButton(QDialogButtonBox::Yes);
    ui->buttonBox->addButton(QDialogButtonBox::No);
}
