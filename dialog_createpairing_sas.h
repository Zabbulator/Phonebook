#ifndef DIALOG_CREATEPAIRING_SAS_H
#define DIALOG_CREATEPAIRING_SAS_H

/**
 * @file dialog_createpairing_sas.h
 * @brief Header of the Dialog_CreatePairing_Sas class
 */

#include "phonebook_global.h"

#include "qzeroconf.h"

#include <QDialog>
#include <QtNetwork>

namespace Ui {
    class Dialog_CreatePairing_Sas;
}

/**
 * @class GenericPairingPhonebook::Dialog_CreatePairing_Sas
 * @brief This class represents the authentication dialog of the pairing process
 *
 * If the user chose to wait for a pairing request with the mDNS-SD method
 * in the previous discovery dialog, this dialog publishes the necessary
 * mDNS-SD service on creation, to allow discovery by the pairing partner.
 * This needs to be done here, because this dialog's exec-call (to prevent
 * access to the MainWindow during the pairing process) blocks the MainWindow's
 * event queue. For the same reason, sending the IPC request to the local
 * pairiang daemon is done here, as waiting for an answer (which will contain
 * the SAS) requires a running event queue (that is not currently blocked)
 */
class GenericPairingPhonebook::Dialog_CreatePairing_Sas : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_CreatePairing_Sas(QZeroConf* zeroConf,
                                      bool isClient,
                                      bool mustPublishMdnssdService,
                                      const QByteArray& pairingPartnerName,
                                      const QByteArray& pairingPartnerIP,
                                      std::uint16_t pairingPartnerPort,
                                      QWidget *parent = 0);
    ~Dialog_CreatePairing_Sas();

#ifdef Q_OS_ANDROID
    QTcpSocket* ipcSocket;
#else
    QLocalSocket* ipcSocket;
#endif // Q_OS_ANDROID

private:
    Ui::Dialog_CreatePairing_Sas *ui;

    QZeroConf* zeroConf;
    bool isClient;
    bool mustPublishMdnssdService;
    QByteArray pairingPartnerName;
    QByteArray pairingPartnerIP;
    std::uint16_t pairingPartnerPort;

    QTimer timer;
    size_t remainingTime;

    /**
     * @brief Sends IPC request to local pairing daemon
     */
    void sendRequest();
    /**
     * @brief Reads answer to IPC request (SAS confirmation)
     */
    void readSasMessage();
};

#endif // DIALOG_CREATEPAIRING_SAS_H
