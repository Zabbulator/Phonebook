#ifndef PHONEBOOK_GLOBAL_H
#define PHONEBOOK_GLOBAL_H

/**
 * @file phonebook_global.h
 * @brief Global header of the phonebook front-end
 *
 * It defines the GenericPairingPhonebook namespace
 */

/**
 * @namespace GenericPairingPhonebook
 * @brief Namespace of the generic pairing daemon's phonebook front-end
 */
namespace GenericPairingPhonebook {
    class MainWindow;
    class Dialog_CreatePairing_Discover;
    class Dialog_CreatePairing_Sas;
};

#endif // PHONEBOOK_GLOBAL_H
